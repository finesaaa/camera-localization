#include <bits/stdc++.h> 
#include <eigen3/Eigen/Dense>

using namespace std; 
using namespace Eigen;

void display(MatrixXd A) 
{ 
     for (int i=0; i < A.rows(); i++) 
	{ 
		for (int j=0; j < A.cols(); j++) 
			cout << A(i,j) << " "; 
		cout << endl; 
	} 
} 

int main() 
{   
	double scale = 1;
     double u = 32, v = 42;
     double f = 77.25, cx = 32, cy = 32;
     double tx = 0, ty = 0, tz = 0;
	MatrixXd img_c(3,1), cam_in(3,3), R(3,3), T(3,1), world_c(3,1);
     MatrixXd suv(3,1), XYZ(3,1);

     cam_in << f, 0, cy,
               0, f, cx,
               0, 0, 1;
     T << tx,
          ty,
          tz;
     R << 1, 0, 0,
          0, 1, 0,
          0, 0, 1;
     img_c << u, 
              v, 
              1;
     
     world_c = cam_in.inverse()*(scale * img_c) - T;
     XYZ = R.inverse()*(world_c);

     display(XYZ);
	return 0; 
} 
