#include<bits/stdc++.h> 
using namespace std; 

#define N 3	 // Number of unknowns (X, Y, Z)

// function to reduce matrix to r.e.f. Returns a value to 
// indicate whether matrix is singular or not 
int forwardElim(double mat[N][N+1]); 

// function to calculate the values of the unknowns 
void backSub(double mat[N][N+1]); 

void gaussianElimination(double mat[N][N+1]) 
{ 
	// Reduce to a echelon form matrix
	int singular_flag = forwardElim(mat); 

	// if matrix is singular 
	if (singular_flag != -1) 
	{ 
		cout << "Singular Matrix.\n";

		if (mat[singular_flag][N]) 
			cout << "Inconsistent System."; 
		else
			cout << "May have infinitely many solutions."; 
		return; 
	} 

	// Get the solution X, Y, Z;
	backSub(mat); 
} 

void swap_row(double mat[N][N+1], int i, int j) 
{ 
	for (int k=0; k<=N; k++) 
	{ 
		double temp = mat[i][k]; 
		mat[i][k] = mat[j][k]; 
		mat[j][k] = temp; 
	} 
} 

void print(double mat[N][N+1]) 
{ 
	for (int i=0; i<N; i++, cout << "\n") 
		for (int j=0; j<=N; j++) 
			cout << mat[i][j] << " "; 

	cout << "\n"; 
} 

int forwardElim(double mat[N][N+1]) 
{ 
	for (int k=0; k<N; k++) 
	{ 
		// Initialize maximum value and index for pivot 
		int i_max = k; 
		int v_max = mat[i_max][k]; 

		for (int i = k+1; i < N; i++) 
			if (abs(mat[i][k]) > v_max) 
				v_max = mat[i][k], i_max = i; 

		/* if a prinicipal diagonal element is zero, it denotes that matrix is singular, and 
		* will lead to a division-by-zero later. */
		if (!mat[k][i_max]) 
			return k; 

		if (i_max != k) 
			swap_row(mat, k, i_max); 

		for (int i=k+1; i<N; i++) 
		{ 
			double f = mat[i][k]/mat[k][k]; 

			for (int j=k+1; j<=N; j++) 
				mat[i][j] -= mat[k][j]*f; 

			mat[i][k] = 0; 
		} 
		//print(mat);	
	} 
	//print(mat);		
	return -1; 
} 

void backSub(double mat[N][N+1]) 
{ 
	double x[N]; // x[0] = X; x[1] = Y; x[2] = Z;

	for (int i = N-1; i >= 0; i--) 
	{ 
		/* start with the RHS of the equation */
		x[i] = mat[i][N]; 

		for (int j=i+1; j<N; j++) 
		{ 
			x[i] -= mat[i][j]*x[j]; 
		} 
		x[i] = x[i]/mat[i][i]; 
	} 

	cout << "\nSolution for the system:" << endl; 
	for (int i=0; i<N; i++) 
		cout << x[i] << endl; 
} 

int main() 
{ 
    double scale = 1.0, u = 32.0, v = 42.0, w = 1.0;
    double Cx, Cy, fx, fy, cek;
    double tx = 0.0, ty = 0.0, tz = 0.0;
    double r[N][N];
	
	fx = 77.25;
	Cx = 0.0;
  fy = fx; 
	Cy = Cx;
    
	for (int i = 0; i < N; i++){
		for (int j = 0; j < N; j++){
			cin >> r[i][j];
		}
	}
    
	double equation[N][N+1]= {
														{r[0][0], r[0][1], r[0][2], ((scale*u)-(Cx*w)-(fx*tx))/fx}, 
														{r[1][0], r[1][1], r[1][2], ((scale*v)-(Cy*w)-(fy*ty))/fy}, 
														{r[2][0], r[2][1], r[2][2], (scale*w)-tz} 
						    					 }; 

	gaussianElimination(equation); 

	return 0; 
} 
